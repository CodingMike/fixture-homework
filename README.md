# Fixture Homework
Vagrant: version 2.2.3
Symfony: version 4.2.10 

Firstly download entire project folder.

Unfortunately, as I made the Symfony app before creating a vagrant box environment to run on, it does not seems to work with the usual process of typing http://192.168.10.10/(filenamehere). This has made it extremely difficult to implement a varant box to the already existing project.

After testing many times I am yet to find a solution to this. However, I assure you I am more than able to run my symfony app on a vagrant box if the entire project was to be re-done.

Ways to access the we page:

OPTION 1: One of the ways to reach my project and call the index.php page (which includes a photo of me utilising bootstrap is to type this in your browser: http://127.0.0.1/fixture/public/index.php

OPTION 2: Alternatively you can type in your we browser: 127.0.0.1 which will bring up the root of the project. From there you will need to navigate to: fixture/public/index.php

The second method is the same as the first but allows you to see the various folders and files rather than going straight to the project which option 1 will make you do.


I apologise for the vagrant box not hosting my web page, I am happy to re-do the project. I wanted to give you something rather than nothing for now as I don't want you to wait too long for me since you already extended my deadline due to my travels, which is much appreciated.

OVERVIEW:

I have made a Symfony app with a twig template that displays a photo of myself and is fully responsive with the use of Bootstrap.
I have run a vagrant box using laravel/Homestead however, I have not managed to syncronise my symfony app on this virtualbox as of yet.

Thank you for your time, and please feel free to contact me if you have any questions regarding the project or beyond.

Kind regards,

Michael Maratheftis
